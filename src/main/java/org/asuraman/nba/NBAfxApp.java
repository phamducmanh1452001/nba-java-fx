package org.asuraman.nba;

import javafx.application.Application;
import javafx.stage.Stage;
import org.asuraman.nba.presentation.base.ViewLoader;
import org.asuraman.nba.core.assets.FXMLPath;
import org.asuraman.nba.data.models.Association;

public class NBAfxApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Load the FXML file
        // primaryStage.getIcons().add(new Image("view/nba.png"));

        primaryStage.setTitle("NBAfxApp");

        ViewLoader.showStage(FXMLPath.associationView, primaryStage, new Association());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
