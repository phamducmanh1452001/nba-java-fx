package org.asuraman.nba.core.assets;

public class ImagePath {
    private static final String rootPath = "/images/";

    public static final String nbaBackground = rootPath + "nba_background.jpg";

    public static final String nba = rootPath + "nba.png";
}
