package org.asuraman.nba.core.assets;

public class FXMLPath {

    private static final String rootPath = "/fxml/";

    private static String resolvePathByFileName(String fileName) {
        return "/fxml/" + fileName + ".fxml";
    }

    public static final String errorView = resolvePathByFileName("common/error");
    public static final String associationView = resolvePathByFileName("AssociationView");

    public static final String addTeamToRoundView = resolvePathByFileName("AddTeam");

    public static final String teamsTableView = resolvePathByFileName("TeamsTable");

    public static final String currentRoundTeamsView = resolvePathByFileName("CurrentRoundTeamsView");

    public static final String exploreTeamsView = resolvePathByFileName("ExploreTeamsView");

    public static final String manageTeamView = resolvePathByFileName("ManageTeamView");

    public static final String playersView = resolvePathByFileName("PlayersView");

    public static final String playerUpdateView = resolvePathByFileName("PlayerUpdateView");

    public static final String recordView = resolvePathByFileName("RecordView");

    public static final String seasonRoundView = resolvePathByFileName("SeasonRoundView");

    public static final String seasonView = resolvePathByFileName("SeasonView");
}
