package org.asuraman.nba.core;

public class InputException extends Exception{
    public InputException(String message){
        super(message);
    }
}
