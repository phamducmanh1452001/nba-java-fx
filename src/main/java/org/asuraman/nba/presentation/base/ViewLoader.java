package org.asuraman.nba.presentation.base;

import javafx.fxml.*;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.*;
import javafx.scene.*;
import org.asuraman.nba.core.assets.ImagePath;

import java.io.*;
import java.lang.Runnable;

public class ViewLoader {

    public final static int standardHeight = 600;
    public final static int standardWidth = 900;

    public static <T> void showStage(String fxmlPath, Stage stage, T model) {
        showStage(stage, fxmlPath, model, standardWidth, standardHeight, () -> {
        });
    }

    public static <T> void showNewStage(String fxmlPath, String title, T model) {
        Stage stage = new Stage();
        stage.setTitle(title);
        showStage(stage, fxmlPath, model, standardWidth, standardHeight, () -> {
        });
    }

    public static <T> void showNewStage(String fxmlPath, String title, T model, int width, int height) {
        Stage stage = new Stage();
        stage.setTitle(title);

        showStage(stage, fxmlPath, model, width, height, () -> {
        });
    }

    private static <T> void showStage(Stage stage, String fxmlPath, T model, int width, int height, Runnable onStageClosed)  {
        FXMLLoader loader = new FXMLLoader(Controller.class.getResource(fxmlPath), null, null,
                type -> {
                    try {
                        @SuppressWarnings("unchecked")
                        Controller<T> controller = (Controller<T>) type.newInstance();
                        controller.model = model;
                        controller.stage = stage;
                        return controller;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });

        try {
            Parent root = loader.load();
            stage.setScene(new Scene(root, width, height));
            stage.sizeToScene();
            stage.setOnCloseRequest(e -> onStageClosed.run());

            final Image image = new Image(ViewLoader.class.getResourceAsStream(ImagePath.nba));
            stage.getIcons().add(image);

            stage.show();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("View Loader Exception");
            alert.setContentText(e.getMessage());
            alert.showAndWait();

            e.printStackTrace();
        }
    }
}
