package org.asuraman.nba.presentation.controllers;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.asuraman.nba.core.assets.FXMLPath;
import org.asuraman.nba.data.models.Player;
import org.asuraman.nba.data.models.Team;
import org.asuraman.nba.presentation.base.Controller;
import org.asuraman.nba.presentation.base.ViewLoader;

import java.net.URL;
import java.util.ResourceBundle;

public class ManageTeamController extends Controller<Team> {

    @FXML
    private TableView<Player> playerTableView;

    @FXML
    private TextField teamNameTextField;

    @FXML
    private Button addButton;

    @FXML
    private Button updateButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button closeButton;

    private Player selectedPlayer;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();

    }

    @SuppressWarnings("unchecked")
    private void setLabels() {
        addButton.setText("Add");
        updateButton.setText("Update");
        deleteButton.setText("Delete");
        closeButton.setText("Save & Close");

        setDisableUpdateAndDeleteButtons(true);

        TableColumn<Player, String> playerNameColumn = new TableColumn("Player Name");
        playerNameColumn.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getName())
        );
        playerNameColumn.setMinWidth(200);

        TableColumn<Player, Double> playerCreditColumn = new TableColumn("Player Credit");
        playerCreditColumn.setCellValueFactory(cellData ->
                new SimpleDoubleProperty(cellData.getValue().getCredit()).asObject()
        );
        playerCreditColumn.setMinWidth(200);

        TableColumn<Player, Integer> playerAgeColumn = new TableColumn("Player Age");
        playerAgeColumn.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getAge()).asObject()
        );
        playerAgeColumn.setMinWidth(200);

        TableColumn<Player, Integer> playerNoColumn = new TableColumn("Player No");
        playerNoColumn.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getNo()).asObject()
        );
        playerNoColumn.setMinWidth(200);

        playerTableView.getColumns().addAll(
                playerNameColumn,
                playerCreditColumn,
                playerAgeColumn,
                playerNoColumn
        );

        playerTableView.setItems(this.model.getCurrentPlayers());

        playerTableView.getSelectionModel().selectedItemProperty().addListener((
                ObservableValue<? extends Player> observable,
                Player oldValue,
                Player newValue) -> {
            if (newValue != null) {
                selectedPlayer = newValue;
                setDisableUpdateAndDeleteButtons(false);
            }
        });
        
    }

    @SuppressWarnings("unchecked")
    private void setActions() {
        addButton.setOnAction(event -> {
            ViewLoader.showNewStage(
                    FXMLPath.playerUpdateView,
                        "Add New Player ",
                    new Pair(model, null),
                    600,
                    400
            );
        });
        updateButton.setOnAction(event -> {
            ViewLoader.showNewStage(
                    FXMLPath.playerUpdateView,
                    "Update Player : " + selectedPlayer.getName(),
                    new Pair(model, selectedPlayer),
                    600,
                    400
            );
        });
        deleteButton.setOnAction(event -> {
            if (selectedPlayer != null) {
                this.model.getCurrentPlayers().remove(selectedPlayer);
                selectedPlayer = null;

                setDisableUpdateAndDeleteButtons(true);

                playerTableView.getSelectionModel().clearSelection();
            }
        });
        closeButton.setOnAction(event -> {
            updateTeamName();
        });
    }

    private void updateTeamName() {
        final String updatedTeamName = teamNameTextField.getText();
        if (!updatedTeamName.isEmpty()) {
            this.model.setName(updatedTeamName);
        }
        closeStage();
    }
    private void closeStage() {
        final Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    private void setDisableUpdateAndDeleteButtons(boolean status) {
        updateButton.setDisable(status);
        deleteButton.setDisable(status);
    }
}
