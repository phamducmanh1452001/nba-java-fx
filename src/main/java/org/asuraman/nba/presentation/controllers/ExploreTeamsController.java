package org.asuraman.nba.presentation.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.asuraman.nba.presentation.base.Controller;
import org.asuraman.nba.presentation.base.ViewLoader;
import org.asuraman.nba.core.assets.FXMLPath;
import org.asuraman.nba.core.assets.ImagePath;
import org.asuraman.nba.data.models.Teams;

import java.net.URL;
import java.util.ResourceBundle;

public class ExploreTeamsController extends Controller<Teams> implements Initializable {

    @FXML
    private Button teamsButton;

    @FXML
    private Button playersButton;

    @FXML
    private Button closeButton;

    @FXML
    private ImageView backgroundImageView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    private void setLabels() {
        teamsButton.setText("Teams");
        playersButton.setText("Players");
        closeButton.setText("Close");

        final Image image = new Image(getClass().getResourceAsStream(ImagePath.nbaBackground));
        backgroundImageView.setImage(image);
    }

    private void setActions() {
        final Teams teams = new Teams();
        teamsButton.setOnAction(event -> {
            ViewLoader.showNewStage(FXMLPath.teamsTableView, "Teams View", teams);
        });
        playersButton.setOnAction(event -> {
            ViewLoader.showNewStage(FXMLPath.playersView, "Players", teams, 1200, 600);
        });
        closeButton.setOnAction(event -> {
            final Stage stage = (Stage) closeButton.getScene().getWindow();
            stage.close();
        });
    }
}
