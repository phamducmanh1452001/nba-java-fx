package org.asuraman.nba.presentation.controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.asuraman.nba.core.Validator;
import org.asuraman.nba.core.assets.FXMLPath;
import org.asuraman.nba.data.models.Player;
import org.asuraman.nba.data.models.Players;
import org.asuraman.nba.data.models.Team;
import org.asuraman.nba.presentation.base.Controller;
import org.asuraman.nba.presentation.base.ViewLoader;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PlayerUpdateController
        extends Controller<Pair<Team, Player>> {

    protected enum PlayerUpdateType {
        UPDATE,
        ADD_NEW,
    }

    private final Validator validator = new Validator();

    @FXML
    private Button addButton;

    @FXML
    private Button updateButton;

    @FXML
    private Button closeButton;

    @FXML
    private TextField playerNameTextField;

    @FXML
    private TextField playerCreditTextField;

    @FXML
    private TextField playerAgeTextField;

    @FXML
    private  TextField playerNoTextField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    private void setLabels() {
        addButton.setText("Add");
        updateButton.setText("Update");
        closeButton.setText("Close");

        switch (getUpdateType()) {
            case ADD_NEW:
                playerNameTextField.setText("");
                playerCreditTextField.setText("");
                playerAgeTextField.setText("");
                playerNoTextField.setText("");
                break;
            case UPDATE:
                final Player player = this.model.getValue();
                playerNameTextField.setText(player.getName());
                playerCreditTextField.setText(player.getCredit().toString());
                playerAgeTextField.setText(player.getAge().toString());
                playerNoTextField.setText(player.getNo().toString());
                break;
        }
    }

    private void setActions() {
        switch (getUpdateType()) {
            case ADD_NEW:
                updateButton.setDisable(true);
                break;
            case UPDATE:
                addButton.setDisable(true);
                break;
        }

        addButton.setOnAction(event -> {
            setPlayerByTextFieldValues();
        });

        updateButton.setOnAction(event -> {
            setPlayerByTextFieldValues();
        });

        closeButton.setOnAction(event -> {
            closeStage();
        });
    }

    private void closeStage() {
        final Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    private PlayerUpdateType getUpdateType() {
        if (this.model.getValue() == null) {
            return PlayerUpdateType.ADD_NEW;
        } else {
            return PlayerUpdateType.UPDATE;
        }
    }

    private void setPlayerByTextFieldValues() {
        final String playerName;
        final String playerCredit;
        final String playerAge;
        final String playerNo;

        playerName = playerNameTextField.getText();
        playerCredit = playerCreditTextField.getText();
        playerAge = playerAgeTextField.getText();
        playerNo = playerNoTextField.getText();

        validator.clear();
        validator.generateErrors(
                playerName,
                playerCredit,
                playerAge,
                playerNo
        );

        final List<String> errors = validator.errors();
        if (errors.isEmpty()) {
            Player player = this.model.getValue();
            final Team team = this.model.getKey();

            try {
                switch (getUpdateType()) {
                    case ADD_NEW:
                        player = new Player(
                                playerName,
                                Double.parseDouble(playerCredit),
                                Integer.parseInt(playerAge),
                                Integer.parseInt(playerNo)
                        );
                        team.getCurrentPlayers().add(player);
                        break;
                    case UPDATE:
                        player.setName(playerName);
                        player.setAge(Integer.parseInt(playerAge));
                        player.setCredit(Double.parseDouble(playerCredit));
                        player.setNo(Integer.parseInt(playerNo));

                        ObservableList<Player> players = team.getCurrentPlayers();
                        for (int i = 0; i < players.size(); i++) {
                            if (players.get(i) == player) {
                                players.set(i, player);
                            }
                        }

                        break;
                }
                closeStage();
            } catch (NullPointerException | NumberFormatException exception) {
                showError(exception.getMessage());
            }
        } else {
            showError(errors.stream().reduce("", (previous, current) -> previous + "\n" + current));
        }
    }

    private void showError(String message) {
        String errorTitle = "";
        switch (getUpdateType()) {
            case ADD_NEW:
                errorTitle = "Error!";
                break;
            case UPDATE:
                errorTitle = "Input Errors!";
                break;
        }

        ViewLoader.showNewStage(
                FXMLPath.errorView,
                errorTitle,
                message,
                300,
                400
        );
    }
}
