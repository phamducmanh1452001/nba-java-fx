package org.asuraman.nba.presentation.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.asuraman.nba.presentation.base.Controller;

import org.asuraman.nba.data.models.*;
import java.net.URL;
import java.util.ResourceBundle;

public class TeamsRoundController extends Controller<Season> {
    @FXML
    private TableView<Game> roundTableView;

    @FXML
    private Button arrowButton;

    @FXML
    private Button arrangeSeasonButton;

    @FXML
    private ListView<Team> teamListView;

    @FXML
    private Label roundLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    private void setLabels() {
        arrowButton.setText(">>>");
        arrangeSeasonButton.setText("Arrange Season");
        roundLabel.setText("Round: 1");

//        teamListView.setCellFactory(
//                value -> {
//                    return new ListCell<Team>(
//
//                    );
//                }
//        );

        final ObservableList<Team> currentTeamList = this.model.getCurrentTeams();
        teamListView.setItems(currentTeamList);

        /*
        *
        *   TableColumn<Player, Integer> playerNoColumn = new TableColumn("Player No");
            playerNoColumn.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getNo()).asObject()
            );
            playerNoColumn.setMinWidth(200);
        *
        */

//        final TableColumn<Game, String> teamOneColumn = new TableColumn("Team-1");
//        teamOneColumn.setCellValueFactory(cellData ->
//                new SimpleStringProperty(cellData.getValue().getName())
//        );
//        teamOneColumn.setMinWidth(120);
//
//        final TableColumn<Game, String> teamTwoColumn = new TableColumn("Team-2");
//        teamTwoColumn.setCellValueFactory(cellData ->
//                new SimpleStringProperty(cellData.getValue().getName())
//        );
//        teamTwoColumn.setMinWidth(120);
//
//        roundTableView.getColumns().addAll(
//                teamOneColumn,
//                teamTwoColumn
//        );
    }

    private void setActions() {
        arrangeSeasonButton.setDisable(true);
        arrowButton.setDisable(true);
    }
}
