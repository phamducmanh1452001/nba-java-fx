package org.asuraman.nba.presentation.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.asuraman.nba.core.assets.FXMLPath;
import org.asuraman.nba.data.models.Team;
import org.asuraman.nba.data.models.Teams;
import org.asuraman.nba.presentation.base.Controller;
import org.asuraman.nba.presentation.base.ViewLoader;

import java.net.URL;
import java.util.ResourceBundle;

public class AddTeamController extends Controller<Teams> {

    @FXML
    private TextField teamNameTextField;

    @FXML
    private Button addButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    private void setLabels() {
        addButton.setText("Add");
    }

    private void setActions() {
        addButton.setOnAction(event -> {
            addNewTeamIfNeed();
            closeStage();
        });
    }

    private void addNewTeamIfNeed() {
        final String newTeamName = teamNameTextField.getText();

        if (this.model.hasTeam(newTeamName)) {
            ViewLoader.showNewStage(
                    FXMLPath.errorView,
                    "Error!",
                    newTeamName + " already exists",
                    300,
                    200
            );
        } else {
            this.model.teams.add(new Team(newTeamName));
        }
    }
    private void closeStage() {
        final Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }
}
