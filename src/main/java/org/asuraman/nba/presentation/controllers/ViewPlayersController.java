package org.asuraman.nba.presentation.controllers;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.asuraman.nba.core.Validator;
import org.asuraman.nba.core.assets.FXMLPath;
import org.asuraman.nba.data.models.Player;
import org.asuraman.nba.data.models.Teams;
import org.asuraman.nba.presentation.base.Controller;
import org.asuraman.nba.presentation.base.ViewLoader;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ViewPlayersController extends Controller<Teams> {

    private Validator validator = new Validator();

    @FXML
    private TableView<Player> playerTableView;

    @FXML
    private Button closeButton;

    @FXML
    private TextField levelTextField;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField startAgeTextField;

    @FXML
    private TextField endAgeTextField;

    private ObservableList<Player> playerObservableList;
    private FilteredList<Player> filteredPlayerList;

    private Integer startAge = 0;
    private Integer endAge = 0;

    private String level = "";

    private String name = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    @SuppressWarnings("unchecked")
    private void setLabels() {
        this.playerObservableList = this.model.allPlayersList();
        this.filteredPlayerList = new FilteredList<>(this.playerObservableList);

        // Team Column
        TableColumn<Player, String> teamNameColumn = new TableColumn("Team");
        teamNameColumn.setMinWidth(200);
        teamNameColumn.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getTeam().getName())
        );
        // Player Name Column
        TableColumn<Player, String> playerNameColumn = new TableColumn("Player Name");
        playerNameColumn.setMinWidth(200);
        playerNameColumn.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getName())
        );
        // Player Credit Column
        TableColumn<Player, Double> playerCreditColumn = new TableColumn("Player Credit");
        playerCreditColumn.setMinWidth(200);
        playerCreditColumn.setCellValueFactory(cellData ->
                new SimpleDoubleProperty(cellData.getValue().getCredit()).asObject()
        );
        // Player Age Column
        TableColumn<Player, Integer> playerAgeColumn = new TableColumn("Player Age");
        playerAgeColumn.setMinWidth(200);
        playerAgeColumn.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getAge()).asObject()
        );
        // Player No Column
        TableColumn<Player, Integer> playerNoColumn = new TableColumn("Player No");
        playerNoColumn.setMinWidth(200);
        playerNoColumn.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getAge()).asObject()
        );
        // Player Level Column
        TableColumn<Player, String> playerLevelColumn = new TableColumn("Player Level");
        playerLevelColumn.setMinWidth(200);
        playerLevelColumn.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getLevel())
        );

        playerTableView.getColumns().addAll(
                teamNameColumn,
                playerNameColumn,
                playerCreditColumn,
                playerAgeColumn,
                playerNoColumn,
                playerLevelColumn
        );

        playerTableView.setItems(this.filteredPlayerList);

        closeButton.setText("Close");
    }

    private void setActions() {
        levelTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            this.level = newValue;
            this.filteredPlayerList.setPredicate(player -> player.getLevel()
                    .toUpperCase()
                    .contains(newValue.toUpperCase()));
        });

        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            this.name = newValue;
            this.filteredPlayerList.setPredicate(player -> player.getName()
                    .toUpperCase()
                    .contains(newValue.toUpperCase()));
        });

        startAgeTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) {
                this.startAge = 0;
                reloadTableView();
                return;
            }
            try {
                final Integer startAge = Integer.parseInt(newValue);

                this.startAge = startAge;

                reloadTableView();
            } catch (NullPointerException | NumberFormatException exception) {
                showError(exception.getMessage());
            }
        });

        endAgeTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) {
                this.endAge = 0;
                reloadTableView();
                return;
            }
            try {
                final Integer endAge = Integer.parseInt(newValue);
                this.endAge = endAge;

                reloadTableView();
            } catch (NullPointerException | NumberFormatException exception) {
                showError(exception.getMessage());
            }
        });

        closeButton.setOnAction(event -> {
            closeStage();
        });
    }

    private  void reloadTableView() {
        this.filteredPlayerList.setPredicate(
                player -> {
                    final boolean ageCondition = (player.getAge() >= this.startAge || this.startAge == 0)
                            && (player.getAge() <= this.endAge || this.endAge == 0);
                    final boolean nameAndLevelCondition = player.getLevel()
                            .toUpperCase()
                            .contains(this.level.toUpperCase())

                            &&

                            player
                            .getName()
                            .toUpperCase()
                            .contains(this.name.toUpperCase());

                    return ageCondition && nameAndLevelCondition;
                }
        );

    }
    private void closeStage() {
        final Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    private void showError(String message) {
        ViewLoader.showNewStage(
                FXMLPath.errorView,
                "Error",
                "Invalid Input: " + message,
                300,
                400
        );
    }
}
