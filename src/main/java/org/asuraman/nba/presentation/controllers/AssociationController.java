package org.asuraman.nba.presentation.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.asuraman.nba.presentation.base.Controller;
import org.asuraman.nba.presentation.base.ViewLoader;
import org.asuraman.nba.core.assets.FXMLPath;
import org.asuraman.nba.core.assets.ImagePath;
import org.asuraman.nba.data.models.Association;
import org.asuraman.nba.data.models.Season;
import org.asuraman.nba.data.models.Teams;

import java.net.URL;
import java.util.ResourceBundle;

public class AssociationController extends Controller<Association> {
    @FXML
    private Button exploreTeamsButton;

    @FXML
    private Button arrangeNewSeason;

    @FXML
    private Button exitButton;

    @FXML
    private ImageView backgroundImageView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    private void setLabels() {
        exploreTeamsButton.setText("Explore the teams");
        arrangeNewSeason.setText("Arrange a new season");
        exitButton.setText("Exit");

        final Image image = new Image(getClass().getResourceAsStream(ImagePath.nbaBackground));
        backgroundImageView.setImage(image);

    }

    private void setActions() {
        arrangeNewSeason.setOnAction(event -> {
            ViewLoader.showNewStage(FXMLPath.seasonView, "Season View", new Season(), 400, 720);
        });
        exploreTeamsButton.setOnAction(event -> {
            ViewLoader.showNewStage(FXMLPath.exploreTeamsView, "Explore Teams", new Teams());
        });
        exitButton.setOnAction(event -> {
            final Stage stage = (Stage) exitButton.getScene().getWindow();
            stage.close();
        });
    }
}
