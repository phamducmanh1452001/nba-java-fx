package org.asuraman.nba.presentation.controllers;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.asuraman.nba.presentation.base.Controller;
import org.asuraman.nba.presentation.base.ViewLoader;
import org.asuraman.nba.core.assets.FXMLPath;
import org.asuraman.nba.data.models.Team;
import org.asuraman.nba.data.models.Teams;

import java.net.URL;
import java.util.ResourceBundle;

public class TeamsController extends Controller<Teams> {


    private Team selectedTeam = null;

    @FXML
    private TableView<Team> teamTableView;

    @FXML
    private Button addNewTeamButton;

    @FXML
    private Button mangageTeamButton;

    @FXML
    private Button deleteTeamButton;

    @FXML
    private Button closeButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setLabels();
        setActions();
    }

    @SuppressWarnings("unchecked")
    private void setLabels() {
        addNewTeamButton.setText("Add");
        mangageTeamButton.setText("Manage");
        deleteTeamButton.setText("Delete");

        setDisableManageAndDeleteButtons(true);

        closeButton.setText("Close");

        TableColumn<Team, String> nameColumn = new TableColumn("Team Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        nameColumn.setMinWidth(200);

        TableColumn<Team, Integer> numberOfPlayersColumn = new TableColumn("Number of Players");
        numberOfPlayersColumn.setCellValueFactory(cellData ->
                new SimpleIntegerProperty(cellData.getValue().getPlayers().getPlayersList().size()).asObject()
        );
        numberOfPlayersColumn.setMinWidth(200);

        TableColumn<Team, Double> averagePlayerCreditColumn = new TableColumn("Average Player CreditColumn");
        averagePlayerCreditColumn.setCellValueFactory(cellData ->
                new SimpleDoubleProperty(cellData.getValue().getPlayers().CountAvgCredit()).asObject()
        );
        averagePlayerCreditColumn.setMinWidth(200);

        TableColumn<Team, Double> averageAgeColumn = new TableColumn("Average Age");
        averageAgeColumn.setMinWidth(200);
        averageAgeColumn.setCellValueFactory(cellData ->
                new SimpleDoubleProperty(cellData.getValue().getPlayers().CountAvgAge()).asObject()
        );

        teamTableView.getColumns().addAll(
                nameColumn,
                numberOfPlayersColumn,
                averagePlayerCreditColumn,
                averageAgeColumn
        );

        teamTableView.setItems(this.model.teams);
    }

    private void setActions() {
        addNewTeamButton.setOnAction(event -> {
            ViewLoader.showNewStage(
                    FXMLPath.addTeamToRoundView,
                    "Adding New Team",
                    new Team(""),
                    450,
                    300
            );
        });
        mangageTeamButton.setOnAction(event -> {
            ViewLoader.showNewStage(
                    FXMLPath.manageTeamView,
                    "Manage the team",
                    selectedTeam
            );
        });
        deleteTeamButton.setOnAction(event -> {
            if (selectedTeam != null) {
                this.model.teams.remove(selectedTeam);
                selectedTeam = null;

                setDisableManageAndDeleteButtons(true);

                teamTableView.getSelectionModel().clearSelection();
            }
        });

        closeButton.setOnAction(event -> {
            final Stage stage = (Stage) closeButton.getScene().getWindow();
            stage.close();
        });

        teamTableView.getSelectionModel().selectedItemProperty().addListener((
                ObservableValue<? extends Team> observable,
                Team oldValue,
                Team newValue) -> {
            if (newValue != null) {
                selectedTeam = newValue;
                setDisableManageAndDeleteButtons(false);
            }
        });
    }

    private void setDisableManageAndDeleteButtons(boolean status) {
        mangageTeamButton.setDisable(status);
        deleteTeamButton.setDisable(status);
    }
}
